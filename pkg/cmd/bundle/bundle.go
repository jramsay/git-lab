package bundle

import (
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/jramsay/git-lab/gitlab"
	"gitlab.com/jramsay/git-lab/pkg/cli"
	dump "gitlab.com/jramsay/git-lab/storage/bundle"
	local "gitlab.com/jramsay/git-lab/storage/cache"
)

// Options for creating a cache bundle
type Options struct {
	Path    string
	Project *gitlab.Project

	host      string
	projectID string
}

// Command for creating a cache bundle
func Command() *cobra.Command {
	opts := Options{}
	eg := `
# bundles the cached issues for the gitaly project
git-lab bundle gitaly.tar.gz --project gitlab-org/gitaly --host gitlab.com
`
	cmd := &cobra.Command{
		Use:          "bundle [file]",
		Short:        "Bundles the local project cache into a gzipped tarball",
		Example:      eg,
		SilenceUsage: true,
		Args:         cobra.ExactArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			opts.Path = args[0]

			if opts.projectID != "" {
				project, err := gitlab.NewProjectFromOptions(opts.host, opts.projectID)
				if err != nil {
					return err
				}

				opts.Project = project
			} else {
				project, err := gitlab.NewProjectForRepository(gitlab.NewRepository())
				if err != nil {
					return err
				}

				opts.Project = project
			}

			return bundle(opts)
		},
	}

	cli.AddHostFlag(cmd, &opts.host)
	cli.AddProjectFlag(cmd, &opts.projectID)

	return cmd
}

func bundle(opts Options) error {
	f, err := os.OpenFile(opts.Path, os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}

	cache, err := local.NewCache(nil)
	if err != nil {
		return err
	}
	defer cache.Close()

	return dump.ExportBundle(opts.Project, f, cache)
}
