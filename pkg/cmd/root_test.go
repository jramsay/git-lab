package cmd

import (
	"strings"
	"testing"

	"gitlab.com/jramsay/git-lab/test"
)

func TestCommand_invalid(t *testing.T) {
	out, _, err := test.ExecuteCommand(Root())
	if err == nil {
		t.Errorf("No errors was defined. Output: %s", out)
	}

	expectedPrefix := "Use one of available subcommands"
	if !strings.HasPrefix(err.Error(), expectedPrefix) {
		t.Errorf("Unexpected output: %s", err.Error())
	}
}
