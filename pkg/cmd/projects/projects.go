package projects

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"os"

	"github.com/spf13/cobra"
	gogl "github.com/xanzy/go-gitlab"
	"gitlab.com/jramsay/git-lab/gitlab"
	"gitlab.com/jramsay/git-lab/pkg/cli"
)

// Options for listing projects
type Options struct {
	Host            string
	StarredProjects bool
	OwnedProjects   bool
	Format          string
}

// ListCommand for listing projects
func ListCommand() *cobra.Command {
	opts := Options{}
	eg := `
# lists projects from the host of the current Git repository context
git-lab list projects

# lists starred projects on the host "gitlab.com"
git-lab list projects --host gitlab.com --starred
`
	c := &cobra.Command{
		Use:          "projects",
		Aliases:      []string{"project"},
		Short:        "Lists the projects of a GitLab host",
		Example:      eg,
		SilenceUsage: true,
		Args:         cobra.NoArgs,
		RunE: func(cmd *cobra.Command, args []string) error {
			if !(opts.OwnedProjects || opts.StarredProjects) {
				return errors.New("limit with --owned, --starred, or both")
			}

			return listProjects(cmd.OutOrStdout(), cmd.ErrOrStderr(), opts)
		},
	}

	cli.AddHostFlag(c, &opts.Host)
	cli.AddFormatFlag(c, &opts.Format)

	c.Flags().BoolVarP(&opts.OwnedProjects, "owned", "", true, "filter projects to include only owned projects")
	c.Flags().BoolVarP(&opts.StarredProjects, "starred", "", false, "filter projects to only include starred projects")

	return c
}

func listProjects(out io.Writer, errout io.Writer, opts Options) error {
	projectOpts := gitlab.StreamProjectsOpts{
		Owned:   &opts.OwnedProjects,
		Starred: &opts.StarredProjects,
	}

	// Add 1 more than the default page requested, so queueing doesn't block
	// on a consumer
	projChan := make(chan *gogl.Project, 11)

	go func() {
		err := gitlab.StreamProjects(opts.Host, projectOpts, projChan)
		if err != nil {
			fmt.Fprintln(errout, err)
			os.Exit(1)
		}
	}()

	for p := range projChan {
		switch opts.Format {
		case "json":
			data, err := json.Marshal(p)
			if err != nil {
				return err
			}

			fmt.Fprintln(out, string(data))
		case "text":
			fmt.Fprintf(out, "Project %d: %v/%v\n", p.ID, p.Namespace.Path, p.Path)
		}
	}

	return nil
}
