package issues

import (
	"encoding/json"
	"fmt"
	"io"
	"strconv"

	"github.com/spf13/cobra"
	"gitlab.com/jramsay/git-lab/gitlab"
	"gitlab.com/jramsay/git-lab/internal/index"
	"gitlab.com/jramsay/git-lab/pkg/cli"
	"gitlab.com/jramsay/git-lab/storage/api"
	"gitlab.com/jramsay/git-lab/storage/cache"
)

// Options for viewing, listing and searching issues.
type Options struct {
	IssueID    int
	Project    *gitlab.Project
	ReadCache  bool
	WriteCache bool
	Format     string
	Query      string
	Limit      int

	host      string
	projectID string
}

// ViewCommand for viewing issues
func ViewCommand() *cobra.Command {
	opts := Options{}
	eg := `
# prints issue 23 from the current Git repository context
git-lab view issue 23

# prints issue 52 from the given the short URL on the default host
git-lab view issue gitlab-org/gitaly#52

# prints issue 22 from the given the web URL
git-lab view issue https://gitlab.com/gitlab-org/gitlab/issues/22

# prints issue 1 from the given project and host
git-lab view issue 1 --project gitlab-org/gitlab --host gitlab.com
`
	cmd := &cobra.Command{
		Use:          "issue [id]",
		Short:        "Prints the issue title and description",
		Example:      eg,
		SilenceUsage: true,
		Args:         cobra.ExactArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			// A fully qualified URL takes precedence over project and host flags
			host, projectID, iid, err := gitlab.IssueLinkParts(args[0])
			if err != nil {
				return err
			}

			opts.IssueID = iid

			if host != "" {
				opts.host = host
			}

			if projectID != "" {
				opts.projectID = projectID
			}

			// If project unspecified, use the current Git repository
			if opts.projectID == "" {
				project, err := gitlab.NewProjectForRepository(gitlab.NewRepository())
				if err != nil {
					return err
				}
				opts.Project = project
			}

			if opts.Project == nil {
				project, err := gitlab.NewProjectFromOptions(opts.host, opts.projectID)
				if err != nil {
					return err
				}
				opts.Project = project
			}

			return viewIssue(cmd.OutOrStdout(), opts)
		},
	}

	cli.AddHostFlag(cmd, &opts.host)
	cli.AddProjectFlag(cmd, &opts.projectID)
	cli.AddFormatFlag(cmd, &opts.Format)

	cmd.Flags().BoolVar(&opts.ReadCache, "read-cache", false, "read from the offline cache")
	cmd.Flags().BoolVar(&opts.WriteCache, "write-cache", false, "write to the offline cache")

	return cmd
}

// ListCommand for listing issues
func ListCommand() *cobra.Command {
	opts := Options{}
	eg := `
# lists issues from the current Git repository context
git-lab list issues

# lists issues from the project "gitlab-org/gitlab" on the host "gitlab.com"
git-lab list issues --project gitlab-org/gitlab --host gitlab.com
`
	cmd := &cobra.Command{
		Use:          "issues",
		Aliases:      []string{"issue"},
		Short:        "Lists the projects issues",
		Example:      eg,
		SilenceUsage: true,
		Args:         cobra.NoArgs,
		RunE: func(cmd *cobra.Command, args []string) error {
			// If project unspecified, use the current Git repository
			if opts.projectID == "" {
				project, err := gitlab.NewProjectForRepository(gitlab.NewRepository())
				if err != nil {
					return err
				}
				opts.Project = project
			}

			if opts.Project == nil {
				project, err := gitlab.NewProjectFromOptions(opts.host, opts.projectID)
				if err != nil {
					return err
				}
				opts.Project = project
			}

			return listIssues(cmd.OutOrStdout(), opts)
		},
	}

	cli.AddHostFlag(cmd, &opts.host)
	cli.AddProjectFlag(cmd, &opts.projectID)
	cli.AddFormatFlag(cmd, &opts.Format)

	cmd.Flags().BoolVarP(&opts.ReadCache, "read-cache", "", false, "read from the offline cache")
	cmd.Flags().BoolVarP(&opts.WriteCache, "write-cache", "", false, "write to the offline cache")

	return cmd
}

// SearchCommand for searching issues
func SearchCommand() *cobra.Command {
	opts := Options{}
	eg := `
# searches cached issues for open issues related to the term "concurrency"
git-lab search issues --query "state:opened concurrency"
`
	cmd := &cobra.Command{
		Use:          "issues",
		Aliases:      []string{"issue"},
		Short:        "Searches the cached issues of a project",
		Example:      eg,
		SilenceUsage: true,
		Args:         cobra.NoArgs,
		RunE: func(cmd *cobra.Command, args []string) error {
			// If project unspecified, use the current Git repository
			if opts.projectID == "" {
				project, err := gitlab.NewProjectForRepository(gitlab.NewRepository())
				if err != nil {
					return err
				}
				opts.Project = project
			}

			if opts.Project == nil {
				project, err := gitlab.NewProjectFromOptions(opts.host, opts.projectID)
				if err != nil {
					return err
				}
				opts.Project = project
			}

			return searchIssues(cmd.OutOrStdout(), opts)
		},
	}

	cli.AddHostFlag(cmd, &opts.host)
	cli.AddProjectFlag(cmd, &opts.projectID)
	cli.AddFormatFlag(cmd, &opts.Format)

	cmd.Flags().StringVarP(&opts.Query, "query", "q", "state:opened", "search query")
	cmd.Flags().IntVarP(&opts.Limit, "limit", "n", 20, "limit the number of results")

	return cmd
}

func viewIssue(out io.Writer, opts Options) error {
	var (
		reader gitlab.IssueReader
		writer gitlab.IssueWriter
	)

	if opts.ReadCache || opts.WriteCache {
		cacheOpts := &cache.Options{
			ReadOnly: !opts.WriteCache,
		}

		cache, err := cache.NewCache(cacheOpts)
		if err != nil {
			return err
		}

		if opts.ReadCache {
			reader = cache
		}

		if opts.WriteCache {
			index, err := index.New("issues", &index.Options{ReadOnly: false})
			if err != nil {
				return err
			}
			defer index.Close()

			cache.RegisterHook(index)

			writer = cache
		}
	}

	if !opts.ReadCache {
		client, err := api.NewClient(opts.Project.Host, opts.Project.AccessToken)
		if err != nil {
			return err
		}

		reader = client
	}

	issue, err := reader.GetIssue(opts.Project, opts.IssueID)
	if err != nil {
		return err
	}

	if writer != nil {
		err = writer.UpdateIssue(opts.Project, issue)
		if err != nil {
			return err
		}
	}

	switch opts.Format {
	case "json":
		data, err := json.Marshal(issue)
		if err != nil {
			return err
		}

		fmt.Fprintln(out, string(data))
	case "text":
		fmt.Fprintf(out, issue.Markdown())
	}

	return nil
}

func listIssues(out io.Writer, opts Options) error {
	var (
		reader gitlab.IssuesReader
		writer gitlab.IssuesWriter

		readChan  chan *gitlab.Issue
		writeChan chan *gitlab.Issue
		done      chan struct{}
	)

	if opts.ReadCache || opts.WriteCache {
		cacheOpts := &cache.Options{
			ReadOnly: !opts.WriteCache,
		}

		cache, err := cache.NewCache(cacheOpts)
		if err != nil {
			return err
		}

		if opts.ReadCache {
			reader = cache
		}

		if opts.WriteCache {
			index, err := index.New("issues", &index.Options{ReadOnly: false})
			if err != nil {
				return err
			}
			defer index.Close()

			cache.RegisterHook(index)

			writer = cache
		}
	}

	if !opts.ReadCache {
		client, err := api.NewClient(opts.Project.Host, opts.Project.AccessToken)
		if err != nil {
			return err
		}

		reader = client
	}

	readChan = make(chan *gitlab.Issue, 101)
	go reader.GetIssuesStream(opts.Project, nil, readChan)

	if writer != nil {
		writeChan = make(chan *gitlab.Issue, cap(readChan))
		done = make(chan struct{})

		go writer.UpdateIssuesStream(done, opts.Project, writeChan)
	}

	for issue := range readChan {
		if writer != nil {
			writeChan <- issue
		}

		switch opts.Format {
		case "json":
			data, err := json.Marshal(issue)
			if err != nil {
				return err
			}

			fmt.Fprintln(out, string(data))
		case "text":
			fmt.Fprintln(out, issue.TextRow())
		}
	}

	if writer != nil {
		// Close write channel and wait for writes to succeed
		close(writeChan)
		<-done
	}

	return nil
}

func searchIssues(out io.Writer, opts Options) error {
	index, err := index.New("issues", &index.Options{ReadOnly: true})
	if err != nil {
		return err
	}
	defer index.Close()

	results, err := index.Search(opts.Query, opts.Limit)
	if err != nil {
		return err
	}

	if len(results.Hits) == 0 {
		return fmt.Errorf("no results found for: %s", opts.Query)
	}

	cache, err := cache.NewCache(&cache.Options{ReadOnly: true})
	if err != nil {
		return err
	}

	cache.RegisterHook(index)

	for _, result := range results.Hits {
		id, err := strconv.Atoi(string(result.ID))
		if err != nil {
			return fmt.Errorf("result: %#v, key %q could not be transformed to int: %v", result, result.ID, err)
		}

		issue, err := cache.GetIssue(opts.Project, id)
		if err != nil {
			return err
		}

		fmt.Fprintln(out, issue.TextRow())
	}

	return nil
}
