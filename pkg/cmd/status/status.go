package status

import (
	"fmt"
	"os"
	"text/tabwriter"

	"github.com/spf13/cobra"
	"gitlab.com/jramsay/git-lab/color"
	"gitlab.com/jramsay/git-lab/gitlab"
	"gitlab.com/jramsay/git-lab/pkg/cli"
	"gitlab.com/jramsay/git-lab/storage/api"
)

// Options for fetching the status of a commit
type Options struct {
	Project     *gitlab.Project
	Repository  *gitlab.Repository
	ShortOutput bool

	host      string
	projectID string
	remote    string
	branch    string
	commit    string
}

// Command provides command for linting GitLab CI configurations
func Command() *cobra.Command {
	opts := Options{}
	eg := `
# prints the status of the current commit from the tracking remote branch
git-lab status

# prints the status of the provided commit from the provided remote name
git-lab status --commit c8ca732 origin
`
	c := &cobra.Command{
		Use:          "status",
		Short:        "Prints GitLab commit status information",
		Example:      eg,
		SilenceUsage: true,
		Args:         cobra.MaximumNArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			switch len(args) {
			case 0:
				remote, err := gitlab.RemoteForHEAD()
				if err != nil {
					return err
				}

				opts.remote = remote
			case 1:
				opts.remote = args[0]
			}

			branch, err := getBranch(opts.branch)
			if err != nil {
				return err
			}
			opts.branch = branch

			commit, err := getCommit(opts.commit)
			if err != nil {
				return err
			}
			opts.commit = commit

			opts.Repository = gitlab.NewRepository(
				gitlab.WithRemote(opts.remote),
				gitlab.WithBranch(opts.branch),
				gitlab.WithHEAD(opts.commit),
			)

			// If project unspecified, use the current Git repository
			if opts.projectID == "" {
				project, err := gitlab.NewProjectForRepository(opts.Repository)
				if err != nil {
					return err
				}
				opts.Project = project
			}

			if opts.Project == nil {
				project, err := gitlab.NewProjectFromOptions(opts.host, opts.projectID)
				if err != nil {
					return err
				}
				opts.Project = project
			}

			return commitStatus(opts)
		},
	}

	cli.AddHostFlag(c, &opts.host)
	cli.AddProjectFlag(c, &opts.projectID)

	c.Flags().BoolVarP(&opts.ShortOutput, "short", "s", false, "output format")
	c.Flags().StringVarP(&opts.branch, "branch", "", "", "output format")
	c.Flags().StringVarP(&opts.commit, "commit", "", "", "output format")

	return c
}

func commitStatus(opts Options) error {
	if opts.Project == nil {
		return fmt.Errorf("No project")
	}

	if opts.Repository == nil {
		return fmt.Errorf("No repo")
	}

	client, err := api.NewClient(opts.Project.Host, opts.Project.AccessToken)
	if err != nil {
		return err
	}

	commit, err := client.GetCommit(opts.Project, opts.Repository.HEAD)
	if err != nil {
		return err
	}

	status := string(*commit.Status)
	if status == "" {
		return nil
	}

	exitCode := gitlab.ExitCodeFromStatus(status)

	if opts.ShortOutput == true {
		fmt.Printf("%s", color.CommitStatus(status, status))
	} else {
		mr, err := client.FindMergeRequest(opts.Project, opts.branch)
		if err != nil {
			return err
		}

		if mr != nil {
			mrLabel := fmt.Sprintf("Merge Request !%d", mr.IID)
			fmt.Printf("\n%s\n%s\n\n", color.MergeRequest(mrLabel), color.URL(mr.WebURL))
		}

		pipeline, err := gitlab.FetchLastPipelineByCommit(opts.Project, opts.Repository)
		if err == gitlab.ErrNoPipelines {
			os.Exit(exitCode)
		}
		if err != nil {
			return err
		}

		pipelineLabel := fmt.Sprintf("Pipeline #%d", pipeline.ID)
		projectLabel := fmt.Sprintf("%s/%s", opts.Project.Namespace, opts.Project.Name)
		projectBranch := fmt.Sprintf("#%s", opts.Repository.Branch)

		fmt.Printf("\n%s %s %s\n", color.Pipeline(pipelineLabel), color.Project(projectLabel), color.Ref(projectBranch))
		fmt.Printf("%s\n\n", color.URL(pipeline.URL))
		fmt.Printf("%s\n\n", color.CommitStatus(status, status))

		w := tabwriter.NewWriter(os.Stdout, 0, 0, 1, ' ', 0)
		for _, stage := range pipeline.Stages {
			fmt.Fprintf(w, "%s\t%s\n", stage.Name, color.Description(stage.StatusDescription()))
			for _, job := range stage.Jobs {
				if job.Status == "failed" {
					fmt.Fprintf(w, "  %s %s\n", color.CommitStatus(job.Status, gitlab.GlyphFromStatus(job.Status)), job.Name)
				}
			}
		}
		w.Flush()

		fmt.Println()
	}

	os.Exit(exitCode)

	return nil
}

func getBranch(ref string) (string, error) {
	if ref != "" {
		return ref, nil
	}

	branch, err := gitlab.RevParseAbbrev("HEAD")
	if err != nil {
		return "", gitlab.ErrDetachedHead
	}

	return branch, nil
}

func getCommit(sha string) (string, error) {
	if sha != "" {
		if gitlab.IsCommit(sha) == false {
			return "", fmt.Errorf("Not a valid commit: %s", sha)
		}

		return sha, nil
	}

	commit, err := gitlab.OidForRef("HEAD")
	if err != nil {
		return "", gitlab.ErrNoRepository
	}

	return commit, nil
}
