package lint

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/jramsay/git-lab/gitlab"
	"gitlab.com/jramsay/git-lab/pkg/cli"
	"gitlab.com/jramsay/git-lab/storage/api"
)

// Command for linting GitLab CI configurations
func Command() *cobra.Command {
	opts := lintOptions{}
	eg := `
# lints the .gitlab-ci.yml file of the current Git repository context
git-lab lint

# lints the file .gitlab-ci.yml using the host GitLab.com
git-lab lint .gitlab-ci.yml --host gitlab.com
`
	cmd := &cobra.Command{
		Use:          "lint [file]",
		Short:        "Lints the GitLab CI configuration file",
		Example:      eg,
		SilenceUsage: true,
		Args:         cobra.MaximumNArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			switch len(args) {
			case 1:
				opts.Path = args[0]
			default:
				opts.Path = ".gitlab-ci.yml"
			}

			opts.AccessToken = gitlab.AccessTokenForHost(opts.Host)
			opts.StreamOptions = cli.StreamOptions{
				Out: cmd.OutOrStdout(),
				Err: cmd.ErrOrStderr(),
			}

			return opts.run()
		},
	}

	cli.AddHostFlag(cmd, &opts.Host)
	cli.AddFormatFlag(cmd, &opts.Format)

	return cmd
}

type lintOptions struct {
	StreamOptions cli.StreamOptions

	Host        string
	AccessToken string
	Path        string
	Format      string
}

func (opts lintOptions) run() error {
	client, err := api.NewClient(opts.Host, opts.AccessToken)
	if err != nil {
		return err
	}

	content, err := ioutil.ReadFile(opts.Path)
	if err != nil {
		return err
	}

	results, err := client.Lint(string(content))
	if err != nil {
		return err
	}

	switch opts.Format {
	case "json":
		result := struct {
			Status string   `json:"status"`
			Errors []string `json:"errors"`
		}{
			Status: "valid",
		}

		// Linting returned errors
		if len(results) > 0 {
			result.Errors = results
		}

		data, err := json.Marshal(result)
		if err != nil {
			return err
		}

		fmt.Fprintln(opts.StreamOptions.Out, string(data))
	case "text":
		for _, errMsg := range results {
			fmt.Fprintln(opts.StreamOptions.Out, errMsg)
		}
	}

	// Linting returned errors, kill and signal an error
	if len(results) > 0 {
		os.Exit(1)
	}

	return nil
}
