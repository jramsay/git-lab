package mergerequest

import (
	"encoding/base64"
	"fmt"
	"io"
	"os"
	"path"
	"regexp"
	"strconv"
	"strings"

	"github.com/ianbruene/go-difflib/difflib"
	"github.com/spf13/cobra"
	g "github.com/xanzy/go-gitlab"
	"gitlab.com/jramsay/git-lab/gitlab"
	"gitlab.com/jramsay/git-lab/pkg/cli"
	server "gitlab.com/jramsay/git-lab/storage/api"
)

// CheckoutOptions for checking out merge requests
type CheckoutOptions struct {
	MergeRequestID int
	Repository     *gitlab.Repository
}

type SuggestionOptions struct {
	MergeRequestIID int
	Project         *gitlab.Project
	Host            string
	ProjectID       string
	Branch          string
}

// CheckoutCommand for checking out a merge request locally
func CheckoutCommand() *cobra.Command {
	opts := CheckoutOptions{}
	eg := `
# switches branch to the merge-ref for merge request 23 of the current project
git-lab checkout merge-request 23
`
	cmd := &cobra.Command{
		Use:          "merge-request [id]",
		Aliases:      []string{"mr"},
		Short:        "Checkout the merge request",
		Example:      eg,
		SilenceUsage: true,
		Args:         cli.ExactlyOneIID,
		RunE: func(cmd *cobra.Command, args []string) error {
			opts.MergeRequestID, _ = strconv.Atoi(args[0])
			opts.Repository = gitlab.NewRepository()

			return checkoutMergeRequest(cmd.OutOrStdout(), opts)
		},
	}

	return cmd
}

func SuggestionsCommand() *cobra.Command {
	opts := SuggestionOptions{}
	eg := `
# prints the patches for unapplied suggestions for merge request 23 of the current project
git-lab suggestions 23

# prints patches for unapplied suggestions for merge request 1 from project "gitlab-org/gitlab-ce" on "gitlab.com"
git-lab suggestions 1 --project gitlab-org/gitlab-ce --host gitlab.com
`
	cmd := &cobra.Command{
		Use:          "suggestions [id]",
		Short:        "Print patches for unapplied merge request suggestions",
		Example:      eg,
		SilenceUsage: true,
		Args:         cli.MaximumNIID(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			if opts.ProjectID != "" {
				project, err := gitlab.NewProjectFromOptions(opts.Host, opts.ProjectID)
				if err != nil {
					return err
				}

				opts.Project = project
			} else {
				project, err := gitlab.NewProjectForRepository(gitlab.NewRepository())
				if err != nil {
					return err
				}

				opts.Project = project
			}

			api, err := server.NewClient(opts.Project.Host, opts.Project.AccessToken)
			if err != nil {
				return err
			}

			if len(args) == 1 {
				opts.MergeRequestIID, _ = strconv.Atoi(args[0])
			} else {
				branch, err := getBranch(opts.Branch)
				if err != nil {
					return err
				}
				opts.Branch = branch

				mr, err := api.FindMergeRequest(opts.Project, opts.Branch)
				if err != nil {
					return err
				}
				opts.MergeRequestIID = mr.IID
			}

			return printMergeRequestSuggestions(cmd.OutOrStdout(), api, opts)
		},
	}

	cli.AddHostFlag(cmd, &opts.Host)
	cli.AddProjectFlag(cmd, &opts.ProjectID)
	cli.AddBranchFlag(cmd, &opts.Branch)

	return cmd
}

func checkoutMergeRequest(out io.Writer, opts CheckoutOptions) error {
	if !hasMergeRequestsRefSpec(opts.Repository) {
		fmt.Fprintln(out, "Ref doesn't exist, to fetch refs run `git config --add remote.origin.fetch \"+refs/merge-requests/*:refs/merge-requests/*\" && git fetch origin`")
		os.Exit(1)
	}

	mrRefDir := path.Join("refs", "merge-requests")
	refPath := path.Join(mrRefDir, strconv.Itoa(opts.MergeRequestID), "head")
	if !opts.Repository.RefExist(refPath) {
		fmt.Fprintln(out, "Missing ref, fetching...")
		if err := opts.Repository.Fetch(refPath); err != nil {
			return err
		}
	}

	return opts.Repository.Checkout(refPath)
}

func hasMergeRequestsRefSpec(repo *gitlab.Repository) bool {
	for _, line := range repo.ConfigGetAll("remote.origin.fetch") {
		if line == "+refs/merge-requests/*:refs/merge-requests/*" {
			return true
		}
	}

	return false
}

func printMergeRequestSuggestions(out io.Writer, api *server.GitLabAPI, opts SuggestionOptions) error {
	var readChan = make(chan *gitlab.Discussion, 101)
	go api.GetMergeRequestDiscussionsStream(opts.Project, opts.MergeRequestIID, readChan)

	for discussion := range readChan {
		for _, note := range discussion.Notes {
			if !note.System && !note.Resolved && note.Position != nil && note.Position.PositionType == "text" {

				changedLines, ahead, behind := parseSuggestion(note.Body)

				if changedLines == nil {
					continue
				}

				f, _, err := api.Client().RepositoryFiles.GetFile(opts.Project.ID(), note.Position.NewPath, &g.GetFileOptions{Ref: &note.Position.HeadSHA})
				if err != nil {
					return err
				}

				oldPath := "a/" + note.Position.NewPath
				newPath := "b/" + note.Position.NewPath
				oldBytes, _ := base64.StdEncoding.DecodeString(f.Content)
				oldLines := difflib.SplitLines(strings.TrimSuffix(string(oldBytes), "\n"))

				var newLines []string
				for n, line := range oldLines {
					offset := 1 + n - note.Position.NewLine

					if offset == ahead {
						newLines = append(newLines, changedLines...)
						continue
					}

					if offset > ahead && offset < behind+1 {
						continue
					}

					newLines = append(newLines, line)
				}

				diff := difflib.LineDiffParams{
					A:        oldLines,
					B:        newLines,
					FromFile: oldPath,
					ToFile:   newPath,
					Context:  3,
				}
				text, _ := difflib.GetUnifiedDiffString(diff)
				fmt.Printf("diff -u %s %s\n%s\n\n", oldPath, newPath, text)
			}
		}
	}
	return nil
}

// Returns the first suggestion in a comment
func parseSuggestion(note string) ([]string, int, int) {
	var ahead = 0
	var behind = 0
	var suggestion []string
	var isHunk = false

	for _, line := range difflib.SplitLines(note) {
		if strings.HasPrefix(line, "```suggestion:") {
			isHunk = true

			r := regexp.MustCompile(`\-(\d+)\+(\d+)`)
			offsets := r.FindSubmatch([]byte(strings.TrimPrefix(line, "```suggestion:")))

			behind, _ = strconv.Atoi(string(offsets[1]))
			ahead, _ = strconv.Atoi(string(offsets[2]))

			continue
		}

		if strings.HasPrefix(line, "```") {
			return suggestion, ahead, behind
		}

		if isHunk {
			suggestion = append(suggestion, line)
		}
	}

	return nil, 0, 0
}

func getBranch(ref string) (string, error) {
	if ref != "" {
		return ref, nil
	}

	branch, err := gitlab.RevParseAbbrev("HEAD")
	if err != nil {
		return "", gitlab.ErrDetachedHead
	}

	return branch, nil
}
