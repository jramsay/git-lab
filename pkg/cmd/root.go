package cmd

import (
	"fmt"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/jramsay/git-lab/pkg/cmd/bundle"
	"gitlab.com/jramsay/git-lab/pkg/cmd/checkout"
	"gitlab.com/jramsay/git-lab/pkg/cmd/fetch"
	"gitlab.com/jramsay/git-lab/pkg/cmd/lint"
	"gitlab.com/jramsay/git-lab/pkg/cmd/list"
	"gitlab.com/jramsay/git-lab/pkg/cmd/mergerequest"
	"gitlab.com/jramsay/git-lab/pkg/cmd/search"
	"gitlab.com/jramsay/git-lab/pkg/cmd/status"
	"gitlab.com/jramsay/git-lab/pkg/cmd/version"
	"gitlab.com/jramsay/git-lab/pkg/cmd/view"
)

// Root command
func Root() *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "git-lab",
		Short: "CLI for GitLab",
		Long:  ``,
	}

	cmd.AddCommand(
		bundle.Command(),
		checkout.Command(),
		fetch.Command(),
		lint.Command(),
		list.Command(),
		status.Command(),
		search.Command(),
		mergerequest.SuggestionsCommand(),
		view.Command(),
		version.Command(),
	)

	visitCommands(cmd, reconfigureCmdWithSubcmd)

	return cmd
}

func reconfigureCmdWithSubcmd(cmd *cobra.Command) {
	if len(cmd.Commands()) == 0 {
		return
	}

	if cmd.Args == nil {
		cmd.Args = cobra.ArbitraryArgs
	}

	if cmd.RunE == nil {
		cmd.RunE = showSubcommands
	}
}

func showSubcommands(cmd *cobra.Command, args []string) error {
	var strs []string
	for _, subcmd := range cmd.Commands() {
		strs = append(strs, subcmd.Use)
	}
	return fmt.Errorf("Use one of available subcommands: %s", strings.Join(strs, ", "))
}

func visitCommands(cmd *cobra.Command, f func(*cobra.Command)) {
	f(cmd)
	for _, child := range cmd.Commands() {
		visitCommands(child, f)
	}
}
