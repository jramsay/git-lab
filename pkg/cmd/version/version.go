package version

import (
	"fmt"

	"github.com/spf13/cobra"
)

var version = "0.1.0"

func Command() *cobra.Command {
	return &cobra.Command{
		Use:   "version",
		Short: "Print the version of git lab",
		Long:  "Print the version of git lab (gitlab.com/jramsay/git-lab)",
		Run: func(cmd *cobra.Command, _args []string) {
			fmt.Fprintf(cmd.OutOrStdout(), "git lab version %v\n", version)
		},
	}

}
