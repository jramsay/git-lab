package cli

import (
	"fmt"
	"strconv"

	"github.com/spf13/cobra"
)

// ExactlyOneIID returns an error if not exactly one argument is provided, and
// the arg is not an integer greater than zero.
func ExactlyOneIID(cmd *cobra.Command, args []string) error {
	if len(args) != 1 {
		return fmt.Errorf("requires exectly one integer identifier")
	}

	i, err := strconv.Atoi(args[0])
	if err != nil {
		return fmt.Errorf("integer identifier could not be parsed")
	}

	if i < 1 {
		return fmt.Errorf("integer identifier must be 1 or greater")
	}

	return nil
}

func MaximumNIID(n int) cobra.PositionalArgs {
	return func(cmd *cobra.Command, args []string) error {
		if len(args) > n {
			return fmt.Errorf("no more than %d merge request identifier", n)
		}

		for _, a := range args {
			i, err := strconv.Atoi(a)
			if err != nil {
				return fmt.Errorf("merge request identifier '%s' could not be parsed", a)
			}

			if i < 1 {
				return fmt.Errorf("merge request identifier must be 1 or greater")
			}
		}

		return nil
	}
}
