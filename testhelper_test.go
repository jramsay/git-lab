package main_test

import (
	"bytes"
	"errors"
	"os/exec"
	"path"
	"runtime"
)

const gitlabTestExec = "./gitlabtest"

// gitlabCommand wraps a command execution
func gitlabCommand(args ...string) (bytes.Buffer, bytes.Buffer, error) {
	cmd := exec.Command(gitlabTestExec, args...)

	var out bytes.Buffer
	cmd.Stdout = &out

	var err bytes.Buffer
	cmd.Stderr = &err

	if err2 := cmd.Start(); err2 != nil {
		return out, err, err2
	}

	return out, err, cmd.Wait()
}

func buildExecutables() error {
	return exec.Command("go", "build", "-o", gitlabTestExec).Run()
}

func rootDir() string {
	_, currentFile, _, ok := runtime.Caller(0)
	if !ok {
		panic(errors.New("rootDir: calling runtime.Caller failed"))
	}

	return path.Dir(currentFile)
}
