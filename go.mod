module gitlab.com/jramsay/git-lab

require (
	code.gitea.io/git v0.0.0-20190311071110-74d7c14dd4a3
	github.com/BurntSushi/toml v0.3.1
	github.com/Unknwon/com v0.0.0-20190214221849-2d12a219ccaf // indirect
	github.com/blevesearch/bleve v1.0.9
	github.com/cznic/b v0.0.0-20181122101859-a26611c4d92d // indirect
	github.com/cznic/mathutil v0.0.0-20181122101859-297441e03548 // indirect
	github.com/cznic/strutil v0.0.0-20181122101858-275e90344537 // indirect
	github.com/dgraph-io/badger/v2 v2.0.3
	github.com/facebookgo/ensure v0.0.0-20160127193407-b4ab57deab51 // indirect
	github.com/facebookgo/stack v0.0.0-20160209184415-751773369052 // indirect
	github.com/facebookgo/subset v0.0.0-20150612182917-8dac2c3c4870 // indirect
	github.com/fatih/color v1.7.0
	github.com/ianbruene/go-difflib v1.2.0
	github.com/jmhodges/levigo v1.0.0 // indirect
	github.com/lithammer/fuzzysearch v1.0.2
	github.com/mattn/go-colorable v0.0.9 // indirect
	github.com/mattn/go-isatty v0.0.4 // indirect
	github.com/mcuadros/go-version v0.0.0-20190308113854-92cdf37c5b75 // indirect
	github.com/remyoudompheng/bigfft v0.0.0-20190321074620-2f0d2b0e0001 // indirect
	github.com/sirupsen/logrus v1.4.2
	github.com/smartystreets/goconvey v0.0.0-20190306220146-200a235640ff // indirect
	github.com/spf13/cobra v0.0.5
	github.com/stretchr/testify v1.4.0
	github.com/tecbot/gorocksdb v0.0.0-20181010114359-8752a9433481 // indirect
	github.com/xanzy/go-gitlab v0.35.1
	golang.org/x/crypto v0.0.0-20190308221718-c2843e01d9a2
	golang.org/x/oauth2 v0.0.0-20190111185915-36a7019397c4 // indirect
	gopkg.in/src-d/go-git.v4 v4.10.0 // indirect
)

go 1.14
