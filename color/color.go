package color

import (
	"github.com/fatih/color"
)

func CommitStatus(s string, input string) string {
	c := color.New()

	switch s {
	case "success":
		c.Add(color.FgGreen)
	case "failed":
		c.Add(color.FgRed)
	case "running":
		c.Add(color.FgYellow)
	case "manual":
		c.Add(color.FgHiBlack)
	default:
		c.Add(color.Reset)
	}

	f := c.SprintFunc()

	return f(input)
}

func MergeRequest(s string) string {
	c := color.New(color.Bold).SprintFunc()

	return c(s)
}

func Pipeline(s string) string {
	c := color.New(color.Bold).SprintFunc()

	return c(s)
}

func Project(s string) string {
	c := color.New(color.FgHiBlack, color.Bold).SprintFunc()

	return c(s)
}

func Ref(s string) string {
	c := color.New(color.FgHiBlack).SprintFunc()

	return c(s)
}

func IID(i int) string {
	c := color.New(color.Bold).SprintFunc()

	return c(i)
}

func URL(s string) string {
	c := color.New(color.FgCyan, color.Underline).SprintFunc()

	return c(s)
}

func Description(s string) string {
	c := color.New(color.FgHiBlack).SprintFunc()

	return c(s)
}
