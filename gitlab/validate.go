package gitlab

func Lint(p *Project, c string) ([]string, error) {
	results, _, err := p.client.Validate.Lint(c)
	return results.Errors, err
}
