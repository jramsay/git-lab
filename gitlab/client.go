package gitlab

import (
	"strings"

	"github.com/xanzy/go-gitlab"
	"gitlab.com/jramsay/git-lab/config"
)

// TODO Use LDFLags to set the version
var UserAgent = "jramsay/Lab 0.0.1"

type Host struct {
	Host        string
	AccessToken string
}

type Client struct {
	Host *Host
}

// NewClient does not return a Client from this package, but a go-gitlab.Client
// to make it confusing
func NewClient(h string) (*gitlab.Client, error) {
	if err := validateHost(h); err != nil {
		return nil, err
	}

	baseurl := h
	if !strings.HasPrefix(baseurl, "http") {
		baseurl = "https://" + baseurl
	}

	c, err := gitlab.NewClient(accessTokenForHost(h), gitlab.WithBaseURL(baseurl))
	if err != nil {
		return nil, err
	}

	return c, nil
}

type StreamProjectsOpts struct {
	Owned   *bool
	Starred *bool
}

// Stream projects will list the projects from the GitLab API and stream these
// through the channel, allowing for pagination request not slowing down the
// data processing pipeline
func StreamProjects(host string, opts StreamProjectsOpts, projChan chan *gitlab.Project) error {
	c, err := NewClient(host)
	if err != nil {
		return err
	}

	glOpts := &gitlab.ListProjectsOptions{
		Starred: opts.Starred,
		Owned:   opts.Owned,
		ListOptions: gitlab.ListOptions{
			PerPage: 10,
			Page:    1,
		},
	}

	for {
		ps, resp, err := c.Projects.ListProjects(glOpts)
		if err != nil {
			break
		}

		for _, p := range ps {
			projChan <- p
		}

		if resp.CurrentPage >= resp.TotalPages {
			break
		}

		// Update the page number to get the next page.
		glOpts.Page = resp.NextPage
	}

	close(projChan)
	return err
}

func validateHost(h string) error {
	// Allow unconfigured GitLab.com servers
	if h == "gitlab.com" {
		return nil
	}

	if strings.HasPrefix(h, "http://127.0") {
		return nil
	}

	for _, s := range config.Config.Servers {
		if s.Host == h {
			return nil
		}
	}

	return ErrServerNotConfigured
}

func accessTokenForHost(h string) string {
	for _, s := range config.Config.Servers {
		if s.Host == h {
			return s.AccessToken
		}
	}

	return ""
}

// AccessTokenForHost returns the access token for the host from the git-lab
// config file
func AccessTokenForHost(h string) string {
	return accessTokenForHost(h)
}
