package gitlab

import (
	"testing"
)

func TestNewRepository(t *testing.T) {
	fakeSHA := "00000000"
	r := NewRepository(WithHEAD(fakeSHA))

	if r.HEAD != fakeSHA {
		t.Fatal("failed to set repo options")
	}
}

func TestOidForRef(t *testing.T) {
	sha, err := OidForRef("HEAD")
	if err != nil {
		t.Fail()
	}
	if len(sha) != 40 {
		t.Fail()
	}
}

func TestConfigGet(t *testing.T) {
	repo := NewRepository(WithRemote("origin"))

	bare := repo.ConfigGet("core.bare")
	if bare != "false" {
		t.Fail()
	}
}
