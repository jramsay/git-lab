package gitlab

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestParseIssueLink_invalid(t *testing.T) {
	_, _, _, err := IssueLinkParts("-5")
	require.NotNil(t, err.Error())

	_, _, _, err = IssueLinkParts("foobar")
	require.NotNil(t, err.Error())
}

func TestParseIssueLink_iid(t *testing.T) {
	h, p, iid, err := IssueLinkParts("4")
	require.NoError(t, err)
	require.Equal(t, 4, iid)
	require.Equal(t, "", h)
	require.Equal(t, "", p)
}

func TestParseIssueURL_short(t *testing.T) {
	h, p, iid, err := IssueLinkParts("group/project#75")
	require.NoError(t, err)
	require.Equal(t, 75, iid)
	require.Equal(t, "", h)
	require.Equal(t, "group/project", p)
}

func TestParseIssueURL_url(t *testing.T) {
	h, p, iid, err := IssueLinkParts("foo://example.com/group/project/issues/50")
	require.NoError(t, err)
	require.Equal(t, 50, iid)
	require.Equal(t, "foo://example.com", h)
	require.Equal(t, "group/project", p)
}
