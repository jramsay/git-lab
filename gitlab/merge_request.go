package gitlab

import (
	"github.com/xanzy/go-gitlab"
)

type MergeRequest struct {
	gitlab.MergeRequest
}
