package gitlab

import (
	"fmt"
	"net/url"
	"strconv"
	"strings"
	"time"

	"github.com/xanzy/go-gitlab"
	"gitlab.com/jramsay/git-lab/color"
)

const IssueType = "issue"

type Issue struct {
	gitlab.Issue
}

type IssuesStreamOpts struct {
	UpdatedAfter *time.Time
}

func (i *Issue) Type() string {
	return IssueType
}

type IssueReader interface {
	GetIssue(p *Project, id int) (*Issue, error)
}

type IssuesReader interface {
	GetIssuesStream(p *Project, opts *IssuesStreamOpts, readChan chan *Issue) error
	CountIssues(p *Project, opts *IssuesStreamOpts) int
}

type IssueWriter interface {
	UpdateIssue(p *Project, i *Issue) error
}

type IssuesWriter interface {
	UpdateIssuesStream(done chan struct{}, p *Project, writeWrite chan *Issue) error
}

type IssuesReaderWriter interface {
	IssuesWriter
	IssuesReader
}

func StreamIssues(project *Project, opts *IssuesStreamOpts, r IssuesReader, w IssuesWriter, progress chan int) (int, error) {
	var (
		readChan  = make(chan *Issue, 100)
		writeChan = make(chan *Issue, cap(readChan))
		done      = make(chan struct{})
	)

	go r.GetIssuesStream(project, opts, readChan)
	go w.UpdateIssuesStream(done, project, writeChan)

	count := 0
	for i := range readChan {
		writeChan <- i
		if progress != nil {
			progress <- 1
		}
		count++
	}

	// Close write channel and wait for writes to succeed
	close(writeChan)
	if progress != nil {
		close(progress)
	}
	<-done

	return count, nil
}

// IssueLinkParts returns the available components from the issue URL, issue
// short link, or integer iid. Link formats are parsed preferring the most
// specific format first (URL), to least specific (IID) last.
func IssueLinkParts(s string) (string, string, int, error) {
	if host, projectID, iid, err := ParseURL(s); err == nil {
		return host, projectID, iid, nil
	}

	if projectID, iid, err := ParseShortLink(s); err == nil {
		return "", projectID, iid, nil
	}

	if iid, err := ParseIID(s); err == nil {
		return "", "", iid, nil
	}

	return "", "", 0, fmt.Errorf("invalid issue link: %s", s)
}

// ParseURL returns the host, project id (group/project) and integer IID for
// the provided issue URL. e.g. https://gitlab.com/gitlab-org/gitaly/issues/123
func ParseURL(s string) (host string, projectID string, iid int, err error) {
	var u *url.URL

	u, err = url.Parse(s)
	if err != nil {
		return
	}

	host = fmt.Sprintf("%s://%s", u.Scheme, u.Host)
	if host == "" {
		err = fmt.Errorf("host must not be empty")
		return
	}

	parts := strings.Split(u.Path, "/issues/")
	if len(parts) != 2 {
		err = fmt.Errorf("invalid url path: %s", u.Path)
		return
	}

	projectID = strings.TrimPrefix(parts[0], "/")
	iid, err = ParseIID(parts[1])

	return
}

// ParseShortLink returns the string project id (group/project) and integer
// IID for the provided short link. e.g. gitlab-org/gitaly#123
func ParseShortLink(s string) (projectID string, iid int, err error) {
	parts := strings.Split(s, "#")
	if len(parts) != 2 {
		err = fmt.Errorf("invalid short link: %s", s)
		return
	}

	projectID = parts[0]
	iid, err = ParseIID(parts[1])

	return
}

// ParseIID returns the integer IID if the provided string is and integer and
// meets basic validation criteria
func ParseIID(s string) (int, error) {
	iid, err := strconv.Atoi(s)
	if err != nil {
		return 0, err
	}

	if iid < 1 {
		return iid, fmt.Errorf("invalid iid, must be greater than 0")
	}

	return iid, nil
}

func (i *Issue) TextRow() string {
	return fmt.Sprintf("%s %s %s", color.IID(i.IID), i.Title, color.URL(i.WebURL))
}

func (i *Issue) Markdown() string {
	return fmt.Sprintf("# %s\n\n%s\n", i.Title, i.Description)
}
