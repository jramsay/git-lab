package gitlab

import (
	"errors"
	"fmt"
	"strings"

	g "github.com/xanzy/go-gitlab"
)

type PipelineStage struct {
	Name   string
	Status string
	Jobs   []*Job
}

type Pipeline struct {
	ID     int
	Status string
	URL    string
	Jobs   []*Job
	Stages []*PipelineStage
}

var (
	ErrNoPipelines = errors.New("No pipelines found for this commit on this ref")
)

func FetchPipeline(p *Project, ID int) (*Pipeline, error) {
	// TODO: Pagination... only the first page of results is being used!
	opts := &g.ListJobsOptions{
		ListOptions: g.ListOptions{
			PerPage: 100,
		},
	}
	jobs, _, err := p.client.Jobs.ListPipelineJobs(p.ID(), ID, opts)
	if err != nil {
		return nil, err
	}

	pipeline := Pipeline{
		ID:     ID,
		URL:    fmt.Sprintf("https://%s/%s/%s/pipelines/%d", p.Host, p.Namespace, p.Name, ID),
		Stages: []*PipelineStage{},
		Jobs:   []*Job{},
	}

	stages := make(map[string]struct{})

	for _, j := range jobs {
		if j.Status == "manual" {
			continue
		}

		job := Job{
			ID:         j.ID,
			Name:       j.Name,
			Status:     j.Status,
			Stage:      j.Stage,
			FinishedAt: j.FinishedAt,
		}

		pipeline.Jobs = append(pipeline.Jobs, &job)
		stages[j.Stage] = struct{}{}
	}

	pipeline.Jobs = deduplicateJobs(pipeline.Jobs)

	pipeline.Status = JobsStatus(pipeline.Jobs)

	for s := range stages {
		stage := PipelineStage{
			Name: s,
			Jobs: []*Job{},
		}

		pipeline.Stages = append(pipeline.Stages, &stage)

		for _, j := range pipeline.Jobs {
			if j.Stage == s {
				stage.Jobs = append(stage.Jobs, j)
			}
		}

		stage.Status = JobsStatus(stage.Jobs)
	}

	return &pipeline, nil
}

func FetchLastPipelineByCommit(p *Project, r *Repository) (*Pipeline, error) {
	pipelineID, err := p.pipelineIDBySHA(r.HEAD, r.Branch)
	if err != nil {
		return nil, err
	}

	return FetchPipeline(p, pipelineID)
}

func (s *PipelineStage) StatusDescription() string {
	var counts = map[string]int{}

	for _, j := range s.Jobs {
		counts[j.Status]++
	}

	parts := []string{}
	for status, count := range counts {
		parts = append(parts, fmt.Sprintf("%d %s", count, status))
	}

	return strings.Join(parts, ", ")
}

func (p *Project) pipelineIDBySHA(sha string, ref string) (int, error) {
	// TODO: Pagination... only the first page of results is being used!
	optsPipeline := &g.ListProjectPipelinesOptions{
		Ref: &ref,
		SHA: &sha,
		ListOptions: g.ListOptions{
			PerPage: 1,
		}}

	pipelines, _, err := p.client.Pipelines.ListProjectPipelines(p.ID(), optsPipeline)
	if err != nil {
		return 0, err
	}

	if len(pipelines) == 0 {
		return 0, ErrNoPipelines
	}

	pipelineID := pipelines[0].ID

	return pipelineID, nil
}

func (p *Pipeline) FindJob(name string) *Job {
	return findJob(p.Jobs, name)
}

func findJob(jobs []*Job, name string) *Job {
	for _, j := range jobs {
		if j.Name == name {
			return j
		}
	}
	return nil
}
