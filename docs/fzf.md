# Examples: using `git-lab` with `fzf`

## Offline issue search with fzf selection and preview

`git-lab search` is used for full text searching of descriptions, and other structured query as you type.

```bash
# glo - 'gitlab open'
glo() {
  GITLAB_PREFIX="./git-lab search issues --query"
  GITLAB_PREVIEW="./git-lab view issue --read-cache {-1}"
  INITIAL_QUERY="state:opened"
  FZF_DEFAULT_COMMAND="$GITLAB_PREFIX '$INITIAL_QUERY'" \
    fzf --bind "change:reload:$GITLAB_PREFIX {q} || true" \
        --ansi --phony --query "$INITIAL_QUERY" \
        --delimiter=" " --with-nth=..-2 \
        --preview "$GITLAB_PREVIEW" | \
        awk '{ print $NF }' | open
}
```
