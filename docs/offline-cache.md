# Offline cache

Quick search, listing and analysis operations are possible using the offline cache.

## Get started

1. Use the `fetch` command to fetch all the issues:

    `git-lab fetch --project gitlab-org/gitlab`

    For large projects, the first fetch will be slow.
    Compressed bundles of public issues are available for `gitlab`,
    `gitlab-ee` and `gitaly` projects.
    To use a bundle during first fetch:

    `git-lab fetch --project gitlab-org/gitlab --bundle-url "https://gitlab-inbox.s3.amazonaws.com/gitlab.com/gitlab-org/gitlab-ee/bundle.tar.gz"`

2. To update the cache for a project run `fetch` again:

    `git-lab fetch --project gitlab-org/gitlab`

3. Support for the cache is opt-in where supported using the
`--read-cache` and `--write-cache` flags
when viewing or listing issues.

While fetching, issues will be indexed for full text search.
To disable this for faster import,
set the `GITLAB_CACHE_SKIP_HOOKS` to `true`.

## Examples

### Creating a CSV using jq

Using [jq](https://stedolan.github.io/jq/) is supported using the
`--format=json` to output [newline delimited JSON](http://ndjson.org/).

```bash
git-lab list issues --project gitlab-org/gitlab --format json --read-cache | \
jq -r '. | select(.labels[] | contains("feature")) | [.web_url, .created_at, .closed_at, .state] | @csv' | \
head -n 20
```
