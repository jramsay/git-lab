package main

import (
	"os"

	"gitlab.com/jramsay/git-lab/config"
	"gitlab.com/jramsay/git-lab/internal/log"
	"gitlab.com/jramsay/git-lab/pkg/cmd"
	"gitlab.com/jramsay/git-lab/storage/cache"
)

func main() {
	if err := config.LoadConfig(); err != nil {
		os.Stderr.WriteString(err.Error())
		os.Exit(1)
	}

	log.Debugf("loaded config")

	gitlab := cmd.Root()

	if err := gitlab.Execute(); err != nil {
		os.Stderr.WriteString(err.Error())
		os.Exit(1)
	}

	log.Debugf("closing the cache")
	if err := cache.Close(); err != nil {
		os.Stderr.WriteString(err.Error())
		os.Exit(1)
	}
}
