package index

import (
	"github.com/blevesearch/bleve"
	"github.com/blevesearch/bleve/mapping"
	"gitlab.com/jramsay/git-lab/gitlab"
)

func indexMapping() mapping.IndexMapping {
	indexMapping := bleve.NewIndexMapping()

	issueMapping := bleve.NewDocumentMapping()
	indexMapping.AddDocumentMapping(gitlab.IssueType, issueMapping)

	textFieldMapping := bleve.NewTextFieldMapping()
	textFieldMapping.Analyzer = "en"

	unindexableTextField := bleve.NewTextFieldMapping()
	unindexableTextField.Index = false

	unindexableNumField := bleve.NewNumericFieldMapping()
	unindexableNumField.Index = false

	unindexableDateField := bleve.NewDateTimeFieldMapping()
	unindexableDateField.Index = false

	issueMapping.AddFieldMappingsAt("id", unindexableNumField)
	issueMapping.AddFieldMappingsAt("issue_link_id", unindexableNumField)

	issueMapping.AddFieldMappingsAt("title", textFieldMapping)
	issueMapping.AddFieldMappingsAt("description", textFieldMapping)
	issueMapping.AddFieldMappingsAt("state", textFieldMapping)
	issueMapping.AddFieldMappingsAt("labels", textFieldMapping)

	issueMapping.AddFieldMappingsAt("_links", unindexableTextField)

	issueMapping.AddFieldMappingsAt("retrieved_at", unindexableDateField)

	return indexMapping
}
