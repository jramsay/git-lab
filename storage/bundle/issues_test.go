package bundle

import (
	"fmt"

	g "github.com/xanzy/go-gitlab"
	"gitlab.com/jramsay/git-lab/gitlab"
)

var issue1 = g.Issue{IID: 1, Title: "The jaws that bite, the claws that catch!"}
var issue2 = g.Issue{IID: 2, Title: "The frumious Bandersnatch!"}

var mockIssues = []*gitlab.Issue{
	&gitlab.Issue{issue1},
	&gitlab.Issue{issue2},
}

type MockStorage struct{}

func (mock *MockStorage) GetIssuesStream(p *gitlab.Project, opts *gitlab.IssuesStreamOpts, issuesChan chan *gitlab.Issue) error {

	for _, i := range mockIssues {
		issuesChan <- i
	}

	close(issuesChan)

	return nil
}

func (mock *MockStorage) CountIssues(p *gitlab.Project, opts *gitlab.IssuesStreamOpts) int {
	return 0
}

func (mock *MockStorage) UpdateIssuesStream(done chan struct{}, p *gitlab.Project, issuesChan chan *gitlab.Issue) error {
	defer close(done)

	for i := range issuesChan {
		if isMockIssue(i) == false {
			return fmt.Errorf("unexpected update issue %v", i)
		}
	}

	return nil
}

func isMockIssue(i *gitlab.Issue) bool {
	for _, mockIssue := range mockIssues {
		if mockIssue.IID == i.IID {
			return true
		}
	}
	return false
}
