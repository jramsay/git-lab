package bundle

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"

	"gitlab.com/jramsay/git-lab/gitlab"
)

func TestBundle(t *testing.T) {
	mockStorage := &MockStorage{}

	project, err := gitlab.NewProjectFromOptions("gitlab.com", "example/foo")
	if err != nil {
		t.Fatal(err)
	}

	dir, err := ioutil.TempDir("", t.Name())
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dir)

	tempFile := filepath.Join(dir, "bundle.tar.gz")

	// Create bundle
	if f, err := os.OpenFile(tempFile, os.O_CREATE|os.O_WRONLY, 0644); err != nil {
		t.Fatal(err)
	} else {

		if err := ExportBundle(project, f, mockStorage); err != nil {
			t.Fatal(err)
		}

		f.Close()
	}

	// Read bundle
	if f, err := os.OpenFile(tempFile, os.O_RDONLY, 0644); err != nil {
		t.Fatal(err)
	} else {
		if err := ImportBundle(project, f, mockStorage); err != nil {
			t.Fatal(err)
		}

		f.Close()
	}
}
