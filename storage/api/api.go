package api

import (
	"strings"

	"github.com/xanzy/go-gitlab"
	"gitlab.com/jramsay/git-lab/internal/log"
)

type GitLabAPI struct {
	client *gitlab.Client
}

func NewClient(host string, accessToken string) (*GitLabAPI, error) {
	if !strings.HasPrefix(host, "http") {
		host = "https://" + host
	}

	client, err := gitlab.NewClient(accessToken, gitlab.WithBaseURL(host))
	if err != nil {
		return nil, err
	}

	log.Debugf("Created GitLab API client for %s", host)
	return &GitLabAPI{client}, nil
}

func (c *GitLabAPI) Client() *gitlab.Client {
	return c.client
}
