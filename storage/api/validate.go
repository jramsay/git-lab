package api

// Lint the provided GitLab CI configuration
func (api *GitLabAPI) Lint(val string) ([]string, error) {
	results, _, err := api.client.Validate.Lint(val)
	if err != nil {
		return nil, err
	}

	return results.Errors, nil
}
