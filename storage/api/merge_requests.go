package api

import (
	g "github.com/xanzy/go-gitlab"
	"gitlab.com/jramsay/git-lab/gitlab"
)

func (api *GitLabAPI) FindMergeRequest(p *gitlab.Project, branch string) (*gitlab.MergeRequest, error) {
	apiOpts := &g.ListProjectMergeRequestsOptions{
		SourceBranch: &branch,
	}

	mrs, _, err := api.client.MergeRequests.ListProjectMergeRequests(p.ID(), apiOpts, nil)
	if err != nil || len(mrs) == 0 {
		return nil, err
	}

	mr := &gitlab.MergeRequest{*mrs[0]}
	return mr, nil
}

func (api *GitLabAPI) GetMergeRequestDiscussionsStream(p *gitlab.Project, mr int, discussionChan chan *gitlab.Discussion) error {
	defer close(discussionChan)

	apiOpts := &g.ListMergeRequestDiscussionsOptions{
		Page:    1,
		PerPage: cap(discussionChan),
	}

	for {
		ds, resp, err := api.client.Discussions.ListMergeRequestDiscussions(p.ID(), mr, apiOpts, nil)
		if err != nil {
			break
		}

		for _, d := range ds {
			discussion := &gitlab.Discussion{*d}
			discussionChan <- discussion
		}

		if resp.CurrentPage >= resp.NextPage {
			break
		}

		apiOpts.Page = resp.NextPage
	}

	return nil
}
