package cache

import (
	"io/ioutil"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	g "github.com/xanzy/go-gitlab"
	"gitlab.com/jramsay/git-lab/gitlab"
)

var project gitlab.Project

func setupIssues(cache *Cache) {
	project, _ := gitlab.NewProjectFromOptions("gitlab.com", "example/foo")

	issue1 := g.Issue{IID: 1, Title: "The jaws that bite, the claws that catch!"}
	issue2 := g.Issue{IID: 2, Title: "The frumious Bandersnatch!"}

	cache.UpdateIssue(project, &gitlab.Issue{issue1})
	cache.UpdateIssue(project, &gitlab.Issue{issue2})
}

func TestImplementsIssueReaderInterface(t *testing.T) {
	cache := &Cache{}

	var i interface{} = cache
	_, ok := i.(gitlab.IssueReader)
	if ok != true {
		t.Fatal("expected gitlab.IssueReader interface")
	}
}

func TestImplementsIssueWriterInterface(t *testing.T) {
	cache := &Cache{}

	var i interface{} = cache
	_, ok := i.(gitlab.IssueWriter)
	if ok != true {
		t.Fatal("expected gitlab.IssueWriter interface")
	}
}

func TestImplementsIssuesReaderInterface(t *testing.T) {
	cache := &Cache{}

	var i interface{} = cache
	_, ok := i.(gitlab.IssuesReader)
	if ok != true {
		t.Fatal("expected gitlab.IssuesReader interface")
	}
}

func TestImplementsIssuesWriterInterface(t *testing.T) {
	cache := &Cache{}

	var i interface{} = cache
	_, ok := i.(gitlab.IssuesWriter)
	if ok != true {
		t.Fatal("expected gitlab.IssuesWriter interface")
	}
}

func TestGetIssue(t *testing.T) {
	dir, err := ioutil.TempDir("", t.Name())
	if err != nil {
		t.Fatal(err)
	}

	cache := setupCache(dir)
	defer cache.db.Close()

	setupIssues(cache)

	project, err := gitlab.NewProjectFromOptions("gitlab.com", "example/foo")
	require.NoError(t, err)

	issue, err := cache.GetIssue(project, 1)
	require.NoError(t, err)
	require.Equal(t, 1, issue.IID)
	require.Equal(t, "The jaws that bite, the claws that catch!", issue.Title)
}

func TestGetIssuesStream(t *testing.T) {
	dir, err := ioutil.TempDir("", t.Name())
	if err != nil {
		t.Fatal(err)
	}

	cache := setupCache(dir)
	defer Close()

	setupIssues(cache)

	project, err := gitlab.NewProjectFromOptions("gitlab.com", "example/foo")
	if err != nil {
		t.Fatal(err)
	}

	issueChan := make(chan *gitlab.Issue, 2)
	go cache.GetIssuesStream(project, nil, issueChan)

	count := 0
	for issue := range issueChan {
		count += 1
		switch count {
		case 1:
			if issue.IID != 1 && issue.Title != "The jaws that bite, the claws that catch!" {
				t.Fatal("unexpected issue content")
			}
		case 2:
			if issue.IID != 2 && issue.Title != "The frumious Bandersnatch!" {
				t.Fatal("unexpected issue content")
			}
		default:
			t.Fatalf("unexpected issue, expected 2, got %d", count)
		}
	}
}

func TestCountIssues(t *testing.T) {
	dir, err := ioutil.TempDir("", t.Name())
	if err != nil {
		t.Fatal(err)
	}

	cache := setupCache(dir)
	defer Close()

	setupIssues(cache)

	project, err := gitlab.NewProjectFromOptions("gitlab.com", "example/foo")
	if err != nil {
		t.Fatal(err)
	}

	count := cache.CountIssues(project, nil)
	if count != 2 {
		t.Fatalf("unexpected issues count, expected 2, got %d", count)
	}
}

func TestUpdatedAtMetaData(t *testing.T) {
	dir, err := ioutil.TempDir("", t.Name())
	if err != nil {
		t.Fatal(err)
	}

	cache := setupCache(dir)
	defer Close()

	require.Equal(t, &epoch, cache.GetIssuesUpdatedAt(&project))

	ts := time.Unix(100, 100)
	require.NoError(t, cache.SetIssuesUpdatedAt(&project, &ts))

	// Using Unix here, as time might have a location
	assert.Equal(t, ts.Unix(), cache.GetIssuesUpdatedAt(&project).Unix())
}
